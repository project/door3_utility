DOOR3 utility
============

This module provides various utility functions to work with drupal.

USAGE
=====

Include the door3_utility.inc file with module_load_include('inc', 'door3_utility').
Now you are able to use all the utility functions from this module.

TODO
====
Cover this module with test.
