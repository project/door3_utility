<?php

/**
 * @file
 * Utility functions.
 *
 * Developer! Before adding code to this module, please answer "yes" for 3
 * questions:
 * - Does you functionality cover general needs (not site-specific)?
 * - Does you function works only with Drupal core modules/code?
 * - Does your code provides unique functionality (maybe some functions you need
 * already present in core)?
 *
 * Before usage, add this code:
 *  module_load_include('inc', 'door3_utility');
 *
 * @author Dmitriy Storozhuk <dmitriy.storozhuk@door3.com>
 */

/**
 * Helper function to easy get $entity field value.
 *
 * This is a wrapper around the field_get_items().
 * @param $entity_type string
 * @param $entity object
 * @param $field_name string
 * @param string $value_column
 * The actual holder of the value, examples:
 *  - 'value' for text, number, field_collection and other field types.
 *  - tid for taxonomy id.
 *  - target_id: entity reference...
 * @param bool $multiple
 *
 * @see field_info_field for information about
 *
 * @return mixed
 * Field value or NULL if field empty.
 * Field value can be one of scalar types or array.
 * FALSE field value column not found.
 */
function door3_utility_get_field_value($entity_type, $entity, $field_name, $value_column = 'value', $multiple = FALSE) {
  if (empty($entity_type)) {
    drupal_set_message(t('Undefined $entity_type. in !fn', array('!fn' => __FUNCTION__. '()')), 'error');
    return NULL;
  }

  if (empty($entity)) {
    drupal_set_message(t('Undefined $entity. in !fn', array('!fn' => __FUNCTION__ . '()')), 'error');
    return NULL;
  }

  $field_items = field_get_items($entity_type, $entity, $field_name);

  if (!empty($field_items)) {
    if ($value_column) {
      if (!$multiple) {
        $field_item = reset($field_items);
        if (!isset($field_item[$value_column])) {
          return FALSE;
        }
        return $field_item[$value_column];
      }
      else {
        $return = array();
        $first_item = reset($field_items);
        if (!isset($first_item[$value_column])) {
          return FALSE;
        }
        foreach ($field_items as $field_item) {
          $return[] = $field_item[$value_column];
        }
        return $return;
      }
    }
  }

  return NULL;
}

/**
 * Set the value of the attached form field.
 *
 * Works for single value fields like text, integer
 * @param $value
 * @param string $value_column
 * @param string $langcode
 * @param bool $multiple
 * @return array
 *   Form element.
 */
function door3_utility_set_field_value($value, $value_column = 'value', $langcode = LANGUAGE_NONE,  $multiple = FALSE) {
  $element = array();
  if ($multiple) {
    foreach ($value as $delta => $item) {
      $element[$langcode][$delta][$value_column] = $item;
    }
  }
  else {
    $element[$langcode][0][$value_column] = $value;
  }
  return $element;
}

/**
 * Set the default value of the attached form field.
 *
 * @param $element
 * @param $value
 * @param string $value_column
 * @param bool $multiple
 */
function door3_utility_set_element_default_value(&$element, $value, $value_column = 'value', $multiple = FALSE) {
  $lng = $element['#language'];
  if ($multiple) {
    $element[$lng] = $value;
    foreach ($element[$lng] as $delta => $column) {
      $element[$lng][$delta][$value_column]['#default_value'] = $value[$delta];
    }
  }
  else {
    $element[$lng][0][$value_column]['#default_value'] = $value;
  }
}

/**
 * Mark form element and it's children as disabled.
 *
 * @param $element
 */
function door3_utility_form_element_mark_disabled(&$element) {
  $element['#disabled'] = TRUE;
}

/**
 * The helper function to recursively get deepest array element value.
 *
 * Works only for first element.
 * The function recursively dig to the deepest element and return it.
 * @param $value array
 *
 * @return array|mixed
 */
function door3_utility_array_get_nested_value($value) {
  if (is_array($value)) {
    $value = array_shift($value);
    $return = door3_utility_array_get_nested_value($value);
    if (!is_array($return)) {
      return $return;
    }
  }
  else {
    return $value;
  }
}

/**
 * Filter unique values in taxonomy term reference field array value.
 *
 * @param array $array
 * @return array
 *  Filtered array ready to assign to field.
 */
function door3_utility_unique_taxonomy_term_field($array) {
  if (empty($array)) {
    return;
  }
  $array_rewrite = array();
  $storage = array();
  foreach($array as $item) {
    if (!isset($storage[$item['tid']])) {
      $storage[$item['tid']] = $item['tid'];
      $array_rewrite[] = $item;
    }
  }
  return $array_rewrite;
}

/**
 * Filter unique values in nested array.
 *
 * @param array $array
 * @param array $column
 * @return array
 *  Filtered array ready to assign to a field.
 */
function door3_utility_unique_multiple_field_value($array, $column) {
  if (empty($array)) {
    return;
  }
  $array_rewrite = array();
  $storage = array();
  foreach($array as $item) {
    if (!isset($storage[$item[$column]])) {
      $storage[$item[$column]] = $item[$column];
      $array_rewrite[] = $item;
    }
  }
  return $array_rewrite;
}

/**
 * This simple function is to render block content from specific $module.
 *
 * @param string $module string
 *   Module name where the block is hosted.
 * @param string $delta string
 *  Block delta
 * @param array $params
 *  mode: string
 *    - 'full': Render the full block, including block theme and subject.
 *    - 'plain': render only block content (HTML also will be rendered if )
 *  suppress_title: bool Remove or not block title.
 *  classes: string additional classes string to use with block wrapper.
 *
 * @return string
 */
function door3_utility_render_block($module = '', $delta = '', array $params = array()) {
  $default_params = array(
    'classes' => '',
    'mode' => 'full',
    'suppress_title' => FALSE
  );
  $params += $default_params;

  $html = '';
  switch ($params['mode']) {
    case 'full':
      $block = block_load($module, $delta);
      $block_content = _block_render_blocks(array($block));
      $block_build = _block_get_renderable_array($block_content);
      if ($params['suppress_title']) {
        foreach (element_children($block_build) as $key) {
          if (isset($block_build[$key]['#block'])) {
            $block_build[$key]['#block']->title = '<none>';
            $block_build[$key]['#block']->subject = '<none>';
          }
        }
      }
      $html = $block_build;
      break;

    case 'plain':
      $block = module_invoke($module, 'block_view', $delta);
      $html = $block['content'];
      break;
  }

  $return = array(
    '#prefix' => '<div class="' . $params['classes'] . '">',
    '#suffix' => '</div>',
  );

  if (is_array($html)) {
    $return += $html;
  }

  return $return;
}

/**
 * The utility function.
 *
 * Attach list of fields to entity form.
 * With this function you can attach default form for each fields provided
 * in $field_names.
 *
 * It works like any other entity edit form, except you can chose which field
 * to render.
 * @param $element array
 *  Form Element to attach fields.
 * @param $form array
 *  Root $form element.
 * @param $form_state array
 *   $form_state array from form builder.
 * @param $entity object
 *  Fully loaded Entity (node, user, taxonomy etc.) for who this form will be
 *  used.
 * @param $field_names array
 *  List of machine name field.
 * @param $entity_type string
 *  The type of entity for which this form is build.
 * @param $bundle_name
 *  Entity bundle.
 *
 * @return array
 */
function door3_utility_form_attached_fields(&$element, &$form, &$form_state, $entity, array $field_names, $entity_type, $bundle_name) {
  $form_state[$entity_type] = $form['#'. $entity_type] = $entity;
  $form['#parents'] = array();

  $form['#entity_type'] = $entity_type;
  $form['#bundle'] = $bundle_name;

  foreach ($field_names as $field_name) {
    $langcode = field_language($entity_type, $entity, $field_name);
    $items = field_get_items($entity_type, $entity, $field_name);
    $field = field_info_field($field_name);
    $instance = field_info_instance($entity_type, $field_name, $bundle_name);
    $field_form = field_default_form($entity_type, $entity, $field, $instance, $langcode, $items, $form, $form_state);
    $element += $field_form;
  }

  return $element;
}

/**
 * Un-shift array item at the top of array keeping assoc keys.
 */
if (!function_exists('array_unshift_assoc')) {
  function array_unshift_assoc(&$arr, $key, $val) {
    $arr = array_reverse($arr, true);
    $arr[$key] = $val;
    return array_reverse($arr, true);
  }
}
